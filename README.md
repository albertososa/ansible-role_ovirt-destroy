# Ansible role: ovirt-destroy

Destroys VMs in a oVirt infrastructure.

## Requirements

None.

## Role variables

Available variables are listed below, along with the default values (see `defaults/main.yml`):

```yaml
ovirt_url: https://ovirt.example.org/ovirt-engine/api
```

The URL of the oVirt API to use.

```yaml
ovirt_login:
  username: user@realm
  password: Pa$sw0rd!
```

Set the credentials to connect oVirt API. The username should specify the realm.

```yaml
ovirt_vms: []
```

Set a list with the names of the VMs to be destroyed in the oVirt infrastructure. For example:

```yaml
ovirt_vms:
  - name: VM-example-1
  - name: VM-example-2
```

## Author

Alberto Sosa (info@albertososa.es)
